import yaml
import os
import json
import re


# %% Function used for parsing yaml files
def parse_yaml(file_path):
    """Load list of languages from languages.yml"""
    with open(file_path, 'r') as file:
        data = yaml.load(file, Loader=yaml.SafeLoader)
    return data


# %% Constants
# Define directories
FILE_DIR = os.path.dirname(os.path.realpath(__file__))
PROJECT_DIR = os.path.abspath(os.path.join(FILE_DIR, ".."))
DATA_DIR = os.path.join(PROJECT_DIR, "data")

# Parse actions/actions_config.yml
ACTIONS_CONFIG = parse_yaml(os.path.join(FILE_DIR, "actions_config.yml"))

# Parse data/languages.yml
LANG_INFO = parse_yaml(os.path.join(DATA_DIR, "languages.yml"))
LANG_LIST = LANG_INFO["languages"]
COMPILER_LIST = LANG_INFO["compilers"]

# Parse data/error_type_info.yml
ERROR_TYPE_INFO = parse_yaml(os.path.join(DATA_DIR, "error_type_info.yml"))


# %% Functions
def parse_stack_trace(language: str, stack_trace: str):
    """Parse a stack trace (str) to attempt to find the error type"""
    # Get the error types associated with the specified language
    error_types = get_error_types(language.lower())
    error_found = {"type": "", "class": ""}
    for er_type in error_types:
        if stack_trace.find(er_type["type"]) != -1:
            # Found the error type in the stack trace
            error_found = er_type
            break
    return error_found


def get_error_types(language: str):
    """Get the exception types for a given computer language"""
    # Get the error types associated with the specified language
    if language.lower() in ERROR_TYPE_INFO:
        error_types = ERROR_TYPE_INFO[language.lower()]
    else:
        error_types = []
    return error_types


def write_error_report(data: dict):
    """
    Write the error report to the file specified
    in actions_config.yml
    """
    report_config = ACTIONS_CONFIG["error_report"]
    outfile = report_config["outfile"]
    write_json(data, outfile, append=report_config["append"])
    return outfile


def write_json(data, filename, append=False):
    """Write data to a json file"""
    # Make sure the output directory exists
    path_split = os.path.split(filename)
    if not os.path.isdir(path_split[0]) and len(path_split[0]) > 0:
        # Make the directory for the output file
        os.makedirs(path_split[0])

    if append:
        # Load existing data from the json file and append the current data
        existing_data = load_existing_data(filename)
        existing_data.append(data)
        data = existing_data

    with open(filename, 'w') as file:
        # Write the data to the json file
        json.dump(data, file, indent=2)
        file.write('\n')


def load_existing_data(json_file):
    """Load existing data from json file and convert to a list"""
    if os.path.isfile(json_file):
        with open(json_file, 'r') as file:
            existing_data = json.load(file)
        if not isinstance(existing_data, list):
            # Convert existing data to a list
            existing_data = [existing_data]
    else:
        existing_data = []
    return existing_data


def get_ask_compiler(language: str):
    """Get the value of ask_compiler for the specified language"""
    # Get a list of language names as listed in languages.yml, and attempt
    # to extract
    lang_names = [x["name"] for x in LANG_LIST]
    name = extract_name(language, lang_names)
    if name and name in lang_names:
        # Find the matching index from the lang_names list
        idx = lang_names.index(name)
        return LANG_LIST[idx]["ask_compiler"]
    else:
        # No match found in languages.yml; ask for a compiler by default
        return True


def extract_name(str_in: str, names: list):
    """Extract from input string using a list of potential names"""
    match_str = match_str_from_list(str_in, names)
    if match_str:
        return match_str

    # Match using regex. Finds the first match in the input string
    # ((?<=[^a-zA-Z])|^) is a lookbehind that matches any non-letter character
    # or the beginning of the string
    # ((?=[^a-zA-Z])|$) is a lookahead that matches any non-letter character or
    # the end of the string
    names_regex = [
        "((?<=[^a-zA-Z])|^)%s((?=[^a-zA-Z])|$)" % prepare_name_regex(x)
        for x in names
    ]
    pattern = re.compile('|'.join(names_regex), re.IGNORECASE)
    result = pattern.search(str_in)
    if result:
        # Found a match
        match_str_orig = result.group(0)
        match_str = match_str_from_list(match_str_orig, names, use_regex=True)
        if match_str:
            # Found a match in the list of names
            return match_str
        else:
            # Didn't find the match in the list of names, so just
            # return the result from the regex match
            return match_str_orig
    else:
        return None


def match_str_from_list(str_in, names, use_regex=False):
    """
    Match a string to a list of strings and return the matching
    string in the list
    """
    if str_in in names:
        # Found in list of names (case sensitive)
        # return the name from the list of names
        match_index = names.index(str_in)
        return names[match_index]

    names_lower = [x.lower() for x in names]
    if str_in.lower() in names_lower:
        # Found in list of names (case insensitive)
        # return the name from the list of names
        match_index = names_lower.index(str_in.lower())
        return names[match_index]

    # Resort to regex matching if specified
    if use_regex:
        str_regex = prepare_name_regex(str_in)
        for name in names:
            result = re.match(str_regex, name)
            if result:
                # Found a match
                return name
    return None


def prepare_name_regex(name):
    """Prepare a name to add to the regex pattern to extract"""
    # Characters to escape with a backslash
    if name.startswith("regex: "):
        # Regex pattern was given, extract after "regex: "
        name = name.split("regex: ", 1)[1]
    else:
        # Insert a backslash before any of these characters
        name = escape_chars(name, '[](){}*+?|^$.\\')
        if ' ' in name:
            # Modify the space to accept any number of whitespace, underscore,
            # or dash characters as well
            name = name.replace(' ', '[\\s_-]*')
    return name


def name_match_regex(text, name):
    """Check if the input text matches the name"""
    pattern = prepare_name_regex(text)
    result = re.match(pattern, name)
    if result:
        return True
    else:
        return False


def escape_chars(text, chars):
    """Insert a backslash before each of the specified chars in text"""
    for c in chars:
        if c in text:
            text = text.replace(c, '\\' + c)
    return text
