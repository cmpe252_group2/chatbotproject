# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions

from typing import Any, Text, Dict, List
from rasa_sdk import Action, Tracker
from rasa_sdk.forms import FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet
import yaml
import json
import os
from datetime import datetime
import getpass
import re
import uuid
from actions import utils


class ActionCompatibilityCheck(Action):

    def name(self) -> Text:
        return "action_compatibility_check"

    def run(self, dispatcher, tracker, domain):
        lib = tracker.get_slot('library')
        lang = tracker.get_slot('language')
        if lang is None:
            # Language slot not filled
            return []
        elif lang.lower() == 'java':
            dispatcher.utter_message(
                "https://abi-laboratory.pro/index.php?view=timeline&lang=java&l=" + lib.replace(' ', '-'))
        elif lib and lang.lower() == 'python':
            if lib.lower() == 'rasa':
                dispatcher.utter_message("MatPlotLib: 1.1 <-> 3.2\nTenserFlow: 2.0+\npython: 3.6 <-> 3.8")
            if lib.lower() == 'opencv':
                dispatcher.utter_message("No Dependencies!")
        return []


class ActionLookupIssue(Action):

    def name(self) -> Text:
        return "action_lookup_issue"

    def run(self,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:
        # Read in common code issue yml file
        with open(r'.\actions\common_code_issues.yml') as file:
            codeErrors = yaml.load(file, Loader=yaml.FullLoader)

        # Get User Common Issue entry
        error = next(tracker.get_latest_entity_values("commonIssues"), None)

        # Find solution from codeErrors Dictionary
        defaultResponse = "Hmmmm....I'm not sure I can quickly solve your problem"
        solution = codeErrors.get(error, defaultResponse)

        # Utter solution to code error
        dispatcher.utter_message("{}".format(solution))

        # Set entity to None
        # tracker.set()
        return []


class ResetSlot(Action):

    def name(self):
        return "action_reset_slot"

    def run(self, dispatcher, tracker, domain):
        # Clear all slots
        ValidateCodeErrorForm.filled_slots = {}
        return [
            SlotSet('language', None),
            SlotSet('version', None),
            SlotSet('compiler', None),
            SlotSet('error_type', None),
            SlotSet('description', None),
            SlotSet('stack_trace', None),
            SlotSet('library', None),
            SlotSet('feedback', None),
        ]



class LogFeedback(Action):

    def name(self):
        return "action_log_feedback"

    def run(self, dispatcher, tracker, domain):

        # Get Feedback
        data = tracker.slots['feedback']

        # Create a dictionary to write to the json file
        out_dict = {
            "time_utc": str(datetime.utcnow()),
            "time_local": str(datetime.now()),
            "feedback": data,
        }

        # Write feedback to a json file
        outfile = 'output/feedback_log.json'
        utils.write_json(out_dict, outfile, append=True)

        # Clear Feedback slot
        return [SlotSet('feedback', None)]


class ValidateCodeErrorForm(FormValidationAction):
    """Form for submitting an error report"""
    filled_slots = {}

    def name(self) -> Text:
        """Return the class name"""
        return "validate_code_error_form"

    async def required_slots(
            self,
            slots_mapped_in_domain: List[Text],
            dispatcher: "CollectingDispatcher",
            tracker: "Tracker",
            domain: "DomainDict",
    ) -> List[Text]:
        """Returns slots which the form should fill.

        Args:
            slots_mapped_in_domain: Names of slots of this form which were
                mapped in the domain.
            dispatcher: the dispatcher which is used to
                send messages back to the user.
            tracker: the conversation tracker for the current user.
            domain: the bot's domain.

        Returns:
            Slot names which should be filled by the form. By default it
            returns the slot names which are listed for this form in the domain
            and use predefined mappings.
        """
        slots = slots_mapped_in_domain

        # Check if a compiler should be requested for the given language
        lang = tracker.get_slot("language")
        if lang and not utils.get_ask_compiler(lang):
            # Do not request a compiler for the selected language
            slots.remove("compiler")

        return slots

    @classmethod
    def validate_language(
            cls,
            slot_value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        """"""
        if "language" in cls.filled_slots:
            # Slot was already filled
            return {"language": cls.filled_slots["language"]}
        elif slot_value:
            cls.filled_slots["language"] = slot_value
        return {"language": slot_value}

    @classmethod
    def validate_version(
            cls,
            slot_value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        """"""
        if "version" in cls.filled_slots:
            # Slot was already filled
            return {"version": cls.filled_slots["version"]}
        elif slot_value:
            cls.filled_slots["version"] = slot_value
        return {"version": slot_value}

    @classmethod
    def validate_compiler(
            cls,
            slot_value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        """"""
        if "compiler" in cls.filled_slots:
            # Slot was already filled
            return {"compiler": cls.filled_slots["compiler"]}
        elif slot_value:
            cls.filled_slots["compiler"] = slot_value
        return {"compiler": slot_value}

    @staticmethod
    def validate_stack_trace(
            slot_value: Text,
            dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any],
    ) -> List[Dict]:
        # Parse the stack trace to extract the error type
        error_type = utils.parse_stack_trace(
            tracker.get_slot("language"),
            tracker.get_slot("stack_trace"),
        )

        # Set the error_type slot
        SlotSet('error_type', error_type["type"])

        # Create a dictionary to write to the json file
        # Note: getpass.getuser() only works if the action server is run
        # locally by the user
        out_dict = {
            "user": getpass.getuser(),
            "report_id": str(uuid.uuid4()),
            "time_utc": str(datetime.utcnow()),
            "language": tracker.get_slot("language"),
            "version": tracker.get_slot("version"),
            "compiler": tracker.get_slot("compiler"),
            "description": tracker.get_slot("description"),
            "error_type": error_type["type"],
            "error_class": error_type["class"],
            "stack_trace": tracker.get_slot("stack_trace"),
        }

        # Write the information to the json file
        filename = utils.write_error_report(out_dict)
        dispatcher.utter_message("Saved your error report to: %s" % filename)

        if slot_value:
            return {"stack_trace": slot_value}
        else:
            return {"stack_trace": ""}
