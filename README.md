# ChatbotProject

Repository for our term project

## Get the code
Create a folder, named whatever you want, to hold hte source code and then checkout the code.

  1) mkdir *yourfolder* && cd *youfolder*
  2) git clone git@gitlab.com:cmpe252_group2/chatbotproject.git

## Get external and open source components
  3) cd chatbotproject
  4) python -m venv ./venv
  5) .\venv\Scripts\activate
  6) pip3 install -U pip
  7) pip3 install rasa
  8) pip install matplotlib==3.2
  9) pip install pyyaml

## You should now have a folder structure like this
- *yourfolder*
    - chatbotproject
        - actions
        - data
        - models
        - output
        - results
        - test
        - venv

## Training the models
Use the following steps to train the model:
-  rasa train --augmentation 100

## Running the ChatbotProject
Use the following steps to run the Chatbot:
  1) Open command prompt
  2) Navigate to *yourfolder*\chatbotproject
  3) .\venv\Scripts\activate
  4) rasa run actions
  5) Open second command prompt
  6) Navigate to *yourfolder*\chatbotproject
  7) .\venv\Scripts\activate
  8) rasa shell
  
## Story Evaluation
As far as we know, currently all stories should be working. You can look in the stories.yml in the data folder to see the stories and in the nlu.yml in the data folder for intents. Stories for the most part should be able to run back-to-back and repeated, but you may experience some funny behavior in back-to-back stories or repeated stories. If this is encountered use the /restart command to get the stories to work again.

The actions validate_code_error_form and action_log_feedback create json files in the output folder with information that the user entered.
